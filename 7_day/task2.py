import os
import shutil

directory_size = {}

def sum_of_dir(path):
    this = path.split('\\')[-1]
    size = 0
    for root, dirs, files in os.walk(path):
        for file in files:
            with open(os.path.join(root, file), 'r') as f:
                line = f.readline()
                size += int(line)
        for dir in dirs:
            size += sum_of_dir(os.path.join(root, dir))

        directory_size[path] = size
        return size


with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))

    my_cwd = os.getcwd()
    home = os.path.join(my_cwd, 'tmp')
    if os.path.exists(home):
        shutil.rmtree(home)
    os.mkdir(home)

    current_position = [home]

    for row in rows:
        if row.startswith('$'): # command
            command = row.strip('$ ')
            parts = command.split(' ')
            if parts[0] == 'cd':
                target = parts[1]
                if target == '..':
                    current_position.pop()
                else:
                    if target == '/':
                        current_position = [home]
                    else:
                        current_position.append(target)
            elif parts[0] == 'ls':
                continue
        else: # dir of rile
            if row.startswith('dir'):
                name = row.split()[1]
                os.mkdir('\\'.join(current_position+[name]))
            else:
                size, file_name = row.split()
                with open('\\'.join(current_position+[file_name]), 'w') as f:
                    f.write(size)


sum_of_dir(home)
used = directory_size.get(home)
free = 70000000 - used
needed = 30000000 - free

values = list(directory_size.values())

values.sort()

for value in values:
    if value >= needed:
        print(value)
        break

if os.path.exists(home):
    shutil.rmtree(home)
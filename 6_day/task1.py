with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))
    row = list(rows[0])
    for i in range(len(row) - 3):
        if len(set(row[i:i+4])) == 4:
            print(i+4)
            break
with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))
    row = list(rows[0])
    for i in range(len(row) - 13):
        if len(set(row[i:i+14])) == 14:
            print(i+14)
            break
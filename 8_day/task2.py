def check_item(matrix, i, j):
    left, right, top, bottom = 0,0,0,0
    m = len(matrix)
    n = len(matrix[0])

    item = matrix[i][j]
    for k in range(i-1, -1, -1):
        top += 1
        if matrix[k][j] >= item:
            break
    for k in range(i+1, m):
        bottom += 1
        if matrix[k][j] >= item:
            break

    for k in range(j-1, -1, -1):
        left += 1
        if matrix[i][k] >= item:
            break
    for k in range(j+1, n):
        right += 1
        if matrix[i][k] >= item:
            break

    return left*right*top*bottom



with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))
    matrix = [[int(x) for x in row] for row in rows]
    res = 0
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            res = max(res, check_item(matrix, i, j))

    print(res)

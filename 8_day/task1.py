def check_item(matrix, i, j):
    left, right, top, bottom = True, True, True, True
    m = len(matrix)
    n = len(matrix[0])

    item = matrix[i][j]
    for k in range(0, i):
        if matrix[k][j] >= item:
            top = False
    for k in range(i+1, m):
        if matrix[k][j] >= item:
            bottom = False

    for k in range(0, j):
        if matrix[i][k] >= item:
            left = False
    for k in range(j+1, n):
        if matrix[i][k] >= item:
            right = False

    return any([left,right,top,bottom])



with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))
    matrix = [[int(x) for x in row] for row in rows]
    cnt = 0
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            cnt += check_item(matrix, i, j)

    print(cnt)

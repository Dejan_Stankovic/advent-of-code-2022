def compare_lists(lst1, lst2):
        if type(lst1) == int and type(lst2) == int:
            return -1 if lst1 < lst2 else 1 if lst1 > lst2 else 0

        elif type(lst1) == list and type(lst2) == list:
            i = 0
            while i < min(len(lst1), len(lst2)):
                comp = compare_lists(lst1[i], lst2[i])
                if comp == 0:
                    i += 1
                    continue
                return comp
            return -1 if len(lst1) < len(lst2) else 1 if len(lst1) > len(lst2) else 0
        else:
            if type(lst1) == int:
                res = compare_lists([lst1], lst2)
            else:
                res = compare_lists(lst1, [lst2])
            return res
    

with open('input2.txt', 'r') as f:
    srt = [[[2]], [[6]]]
    segments = f.read().split('\n\n')
    result = 0
    index = 1
    srt = [[[2]],[[6]]]
    for segment in segments:
        for x in segment.split(): srt.append(eval(x)) 

    from functools import cmp_to_key
    res = 1
    srt.sort(key = cmp_to_key(lambda x, y: compare_lists(x, y)))
    for i, el in enumerate(srt, 1):
        if el in [[[2]], [[6]]]:
            res *= i
    print(res)
def compare_lists(lst1, lst2):
        if type(lst1) == int and type(lst2) == int:
            return -1 if lst1 < lst2 else 1 if lst1 > lst2 else 0

        elif type(lst1) == list and type(lst2) == list:
            i = 0
            while i < min(len(lst1), len(lst2)):
                comp = compare_lists(lst1[i], lst2[i])
                if comp == 0:
                    i += 1
                    continue
                return comp
            return -1 if len(lst1) < len(lst2) else 1 if len(lst1) > len(lst2) else 0
        else:
            if type(lst1) == int:
                res = compare_lists([lst1], lst2)
            else:
                res = compare_lists(lst1, [lst2])
            return res
    

with open('input2.txt', 'r') as f:
    segments = f.read().split('\n\n')
    result = 0
    index = 1
    for segment in segments:
        a, b = map(eval, segment.split())
        compare = compare_lists(a,b)
        if compare == -1:
            result += index
        index += 1
    print(result)
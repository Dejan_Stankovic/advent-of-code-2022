with open('input2.txt', 'r') as f:
    rows = f.readlines()
    
    current_sum = 0
    max_sum = 0
    for val in rows:
        val = val.strip()
        if not val:
            max_sum = max(max_sum, current_sum)
            current_sum = 0
        else:
            val = int(val)
            current_sum += val

    print(max_sum)


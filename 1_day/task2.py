with open('input2.txt', 'r') as f:
    rows = f.readlines()
    
    current_sum = 0
    sums = []
    for val in rows:
        val = val.strip()
        if not val:
            sums.append(current_sum)
            current_sum = 0
        else:
            val = int(val)
            current_sum += val

    print(sum(sorted(sums)[-3:]))
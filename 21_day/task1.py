values = {}
operations = {}


with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))

    num_monkeys = 0
    for row in rows:
        num_monkeys += 1
        name, other = row.split(': ')
        try:
            value = int(other)
            values[name] = value
        except:
            if '+' in other:
                operations[name] = (other, ' + ')
            elif '-' in other:
                operations[name] = (other, ' - ')
            elif '/' in other:
                operations[name] = (other, ' / ')
            elif '*' in other:
                operations[name] = (other, ' * ')
            else:
                assert False
    
    while len(values) < num_monkeys:
        for name, operation in operations.items():
            if name in values.keys():
                continue
            expression, operand = operation
            names = expression.split(operand)
            v1 = values.get(names[0], None)
            v2 = values.get(names[1], None)
            if v1==None or v2==None:
                continue
            result = eval(f'{v1}{operand}{v2}')
            values[name] = result
    print(values['root'])
class MyNode:
    def __init__(self, name = None, val=None, operand=None, left=None, right=None) -> None:
        self.val = val
        self.operand = operand
        self.name = name
        self.left = left
        self.right = right

def inorder(root):
    if root.val != None:
        return root.val
    v1 = inorder(root.left)
    op = root.operand
    v2 = inorder(root.right)
    res = eval(f'{v1}{op}{v2}')
    root.val = res
    return res

def inorder2(root):
    if root.left == None and root.right == None:
        return
    try:
        left_side = inorder(root.left)
        op = root.operand.strip()
        target_value = None
        if op=='+':
            target_value = root.val - left_side
        elif op=='-':
            target_value = left_side - root.val
        elif op=='*':
            target_value = root.val / left_side
        elif op=='/':
            target_value = left_side / root.val
        else:
            assert False

        if root.right.name =='humn':
            print('humn:',target_value)
            return
        root.right.val = target_value
        inorder2(root.right)

    except:
        right_side = inorder(root.right)
        op = root.operand.strip()
        target_value = None
        if op=='+':
            target_value = root.val - right_side
        elif op=='-':
            target_value = right_side + root.val
        elif op=='*':
            target_value = root.val / right_side
        elif op=='/':
            target_value = right_side * root.val
        else:
            assert False

        if root.left.name =='humn':
            print('humn:',target_value)
            return
        root.left.val = target_value
        inorder2(root.left)


values = {}
operations = {}

def create_node(root_name):
    operation = operations.get(root_name, None)
    if operation == None:
        value = values.get(root_name)
        return MyNode(name=root_name, val=value)

    expression, operand = operation
    names = expression.split(operand)
    v1 = create_node(names[0])
    v2 = create_node(names[1])

    result = MyNode(name = root_name, operand=operand, left=v1, right=v2)
    return result
    
    


with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))

    num_monkeys = 0
    for row in rows:
        num_monkeys += 1
        name, other = row.split(': ')
        if name =='humn':
            values[name] = None
            continue
        try:
            value = int(other)
            values[name] = value
        except:
            if '+' in other:
                if name == 'root':
                    operations[name] = (other.replace('+','='), ' = ')
                else:
                    operations[name] = (other, ' + ')
            elif '-' in other:
                if name == 'root':
                    operations[name] = (other.replace('-','='), ' = ')
                else:
                    operations[name] = (other, ' - ')
            elif '/' in other:
                if name == 'root':
                    operations[name] = (other.replace('/','='), ' = ')
                else:
                    operations[name] = (other, ' / ')
            elif '*' in other:
                if name == 'root':
                    operations[name] = (other.replace('*','='), ' = ')
                else:
                    operations[name] = (other, ' * ')
            else:
                assert False
    

    root = create_node('root')
    humn_right = True
    try:
        target_value = inorder(root.left)
        root.right.val = target_value
        inorder2(root.right)

    except:
        humn_right = False
        target_value = inorder(root.right)
        root.left.val = target_value
        inorder2(root.left)
    

    



def move_head_tail(x,y, tx,ty):
    if x == tx:
        if abs(y-ty) <= 1: # tail is 1 up, 1 down, or overlapping 
            pass
        else:
            if y > ty: # tail is 2 steps bellow the head
                ty += 1
            else: # tails is 2 steps above the head
                ty -= 1
    elif x > tx:
        if x - tx == 1:
            if abs(y-ty) <= 1:
                pass
            else:
                tx += 1
                if y > ty:
                    ty += 1
                else:
                    ty -= 1
        elif x - tx == 2:
            tx += 1
            if y > ty:
                ty += 1
            elif y < ty:
                ty -= 1
            else:
                pass # the same row, head was just 2 steps right
        else:
            print('ERROR')

    elif x < tx:
        if x - tx == -1:
            if abs(y-ty) <= 1:
                pass
            else:
                tx -= 1
                if y > ty:
                    ty += 1
                else:
                    ty -= 1
        elif x - tx == -2:
            tx -= 1
            if y > ty:
                ty += 1
            elif y < ty:
                ty -= 1
            else:
                pass # the same row, head was just 2 steps right
        else:
            print('ERROR')

    else:
        print('ERROR INPUT')

    return tx, ty

with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))
    
    tail_visit_set = {(0,0)}

    x, y = 0, 0
    tails = [[0,0] for _ in range(9)]

    for row in rows:
        direction, move = row.split()
        move = int(move)
        for _ in range(move):
            if direction == 'R':
                x += 1
            elif direction == 'L':
                x -= 1
            elif direction == 'U':
                y += 1
            elif direction == 'D':
                y -= 1

            tx, ty = move_head_tail(x, y, *tails[0])
            tails[0] = [tx, ty]
            for i in range(0, 8):
                tx, ty = move_head_tail(*tails[i], *tails[i+1])
                tails[i+1] = [tx,ty]

            

            tail_pos = (tails[-1][0], tails[-1][1])
            tail_visit_set.add(tail_pos)

    print(len(tail_visit_set))

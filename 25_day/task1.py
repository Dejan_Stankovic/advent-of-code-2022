def snafu_to_decade(ss):
    ss = ss[::-1]
    res = 0
    for index, el in enumerate(ss):
        pf = 5**index
        if el == '-':
            res -= pf
        elif el == '=':
            res -= 2*pf
        else:
            res += int(el)*pf
    return res


def max_val_with_pf(pf):
    if pf==0: 
        return 0
    return pf*2 + max_val_with_pf(pf//5)

val_to_char = {2: '2', 1: '1', 0: '0', -1: '-', -2: '='}

def decade_to_snafu(num,pf):
    val = val_to_char.get(num, None)
    if val:
        return val
    mvwpf = max_val_with_pf(pf//5)
    for d in range(-2,3):
        if abs(num-pf*d) <= mvwpf:
            return val_to_char[d] + decade_to_snafu(num-pf*d, pf//5)
    print('error')



with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))
    res = 0
    for row in rows:
        val = snafu_to_decade(row)
        res += val
    print(res)

    pf = 1
    while pf < res:
        pf *= 5
    print(decade_to_snafu(res, pf)[1:]) #leading zero, idk why :D
with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))
    original_lst = []
    copy_lst = []

    for i, row in enumerate(rows):
        a = int(row)
        original_lst.append((i,a))
        copy_lst.append((i,a))
    
    n = len(original_lst)
    for i, a in original_lst:
        for index, (ii, _) in enumerate(copy_lst):
            if ii == i:
                break
        item = copy_lst.pop(index)
        position = index + a

        if position > 0:
            position %= (n-1)
        if position < 0:
            position %= (n-1)
            position -= (n-1)
        copy_lst.insert(position, item)
        
    for ii in range(n):
        if copy_lst[ii][1] == 0:
            break
    i,j,k = (ii+1000)%n, (ii+2000)%n, (ii+3000)%n

    print(copy_lst[i][1] + copy_lst[j][1] + copy_lst[k][1])
    

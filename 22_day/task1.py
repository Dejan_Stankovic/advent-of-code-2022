def print_matrix(matrix):
    for row in matrix:
        for el in row:
            print(el,end='')
        print()


with open('input2.txt', 'r') as f:
    rows = f.readlines()
    
    matrix = []
    instructions = []
    is_newline = False
    for row in rows:
        if not row.strip():
            is_newline = True
            continue
        if not is_newline:
            matrix.append(list(row.strip('\n')))
        else:
            number = 0
            for el in list(row):
                try:
                    el = int(el)
                    number*=10
                    number+=el
                except:
                    instructions.append(number)
                    number = 0
                    instructions.append(el)
            else:
                instructions.append(number)
    maxxx = max(len(matrix[i]) for i in range(len(matrix)))
    for i in range(len(matrix)):
        matrix[i] = matrix[i] + [' ' for _ in range(maxxx-len(matrix[i]))]

    x,y = start = (0, matrix[0].index('.'))
    current_direction_sign = '>'
    matrix[x][y] = current_direction_sign # '>'


    for instr in instructions:
        if type(instr) == int:
            for _ in range(instr):
                if current_direction_sign == '>':
                    xx,yy = x, y+1
                    if yy == len(matrix[xx]) or matrix[xx][yy] == ' ':
                        for ii in range(len(matrix[xx])):
                            if matrix[xx][ii] != ' ':
                                yy = ii
                                break
                elif current_direction_sign == '<':
                    xx,yy = x, y-1
                    if yy < 0 or matrix[xx][yy] == ' ':
                        for ii in range(len(matrix[xx])-1, -1, -1):
                            if matrix[xx][ii] != ' ':
                                yy = ii
                                break
                elif current_direction_sign == 'v':
                    xx,yy = x+1, y
                    if xx == len(matrix) or not matrix[xx][yy].strip():
                        for ii in range(len(matrix)):
                            if matrix[ii][yy].strip():
                                xx = ii
                                break
                elif current_direction_sign == '^':
                    xx,yy = x-1, y
                    if xx < 0 or matrix[xx][yy] == ' ':
                        for ii in range(len(matrix)-1, -1, -1):
                            if matrix[ii][yy] != ' ':
                                xx = ii
                                break


                if matrix[xx][yy] in '><v^.':
                    matrix[xx][yy] = current_direction_sign
                    x,y = xx,yy
                elif matrix[xx][yy] == '#':
                    pass


        elif type(instr) == str:
            if current_direction_sign == 'v':
                if instr=='R':
                    current_direction_sign = '<'
                elif instr == 'L':
                    current_direction_sign = '>'
            
            elif current_direction_sign == '>':
                if instr=='R':
                    current_direction_sign = 'v'
                elif instr == 'L':
                    current_direction_sign = '^'

            elif current_direction_sign == '^':
                if instr=='R':
                    current_direction_sign = '>'
                elif instr == 'L':
                    current_direction_sign = '<'

            elif current_direction_sign == '<':
                if instr=='R':
                    current_direction_sign = '^'
                elif instr == 'L':
                    current_direction_sign = 'v'
            else:
                assert False
            matrix[x][y] = current_direction_sign
        else:
            assert False

    print((x+1) * 1000 + (y+1)*4 + (1 if current_direction_sign=='v' else 2 if current_direction_sign == '<' else 3 if current_direction_sign=='^' else 0))
def print_matrix(matrix):
    for i in range(10):
        for j in range(450, 550):
            print(matrix[i][j], end='')
        print('\n')

def populate_one_piece(matrix, x, y):
    if x < 0 or x > len(matrix[0]):
        return False
    if y+1 == len(matrix):
        return False
    if matrix[y+1][x] == '.':
        return populate_one_piece(matrix, x, y+1)
    elif matrix[y+1][x] in ['O','#']:
        if matrix[y+1][x-1] == '.':
            return populate_one_piece(matrix, x-1, y+1)
        elif matrix[y+1][x+1] == '.':
            return populate_one_piece(matrix, x+1, y+1)
        else:
            if matrix[y][x] == 'O':
                return False
            matrix[y][x] = 'O'
            return True
    else:
        print('error')
        return False
    
with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))
    walls = []
    max_x, max_y = 0, 0
    for row in rows:
        one_wall = [tuple(map(int,point.split(','))) for point in row.split(' -> ') ]
        max_x = max(max_x, *[wall[0] for wall in one_wall])
        max_y = max(max_y, *[wall[1] for wall in one_wall])
        walls.append(one_wall)
    
    # add an infinite wall at the +2 y coordinate
    walls.append([(0, max_y+2), (max_x*2-1, max_y+2)])
                    
    matrix = [['.' for _ in range(max_x*2) ]for _ in range(max_y+1+2)]

    for wall in walls:
        y,x = wall[0]
        for y1, x1 in wall[1:]:
            
            a,b = sorted([x,x1])
            for i in range(a, b+1):
                matrix[i][y] = '#'

            a,b = sorted([y,y1])
            for j in range(a, b+1):
                matrix[x][j] = '#'
            x,y = x1,y1

    cnt = 0
    while populate_one_piece(matrix, 500, 0):
        cnt += 1

    print(cnt)
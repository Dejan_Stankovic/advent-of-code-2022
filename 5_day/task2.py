with open('input2.txt', 'r') as f:
    rows = list(f.readlines())
    is_stack_data = True
    stacks = []
    cleaned_stacks = []
    for row in rows:
        if not row.strip():
            is_stack_data = False
            for index, el in enumerate(stacks[-1]):
                if el.strip():
                    position = int(el)
                    current_stack = []
                    for stack in stacks[:-1]:
                        if stack[index].strip():
                            current_stack.insert(0, stack[index])
                    cleaned_stacks.append(current_stack)
            continue
        if is_stack_data:
            stacks.append(list(row))
        else:
            val, _from, _to = map(int, row.split()[1::2])
            _from -= 1
            _to -= 1

            target_list = cleaned_stacks[_from][-val:]
            cleaned_stacks[_from] = cleaned_stacks[_from][:-val]

            cleaned_stacks[_to] += target_list

    res = []
    for stack in cleaned_stacks:
        res.append(stack[-1])

    print(''.join(res))
with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))
    cycle = 0
    reg_x = 1
    result = 0
    for row in rows:
        op, *val = row.split()        

        if op == 'noop':
            cycle += 1
            if cycle % 40 == 20:
                    result += cycle*reg_x
        else:
            val = int(val[0])
            for _ in range(2):
                cycle += 1
                if cycle % 40 == 20:
                    result += cycle*reg_x

            reg_x += val
    print(result)
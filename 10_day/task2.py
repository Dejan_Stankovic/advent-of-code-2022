with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))
    cycle = 0
    reg_x = 1
    matrix = [['.' for _ in range(40)] for _ in range(6)]

    for row in rows:
        op, *val = row.split()
        

        if op == 'noop':
            if abs(reg_x - cycle%40) <= 1:
                matrix[cycle//40][cycle%40] = '#'
            cycle += 1

        else:
            val = int(val[0])
            for _ in range(2):
                if abs(reg_x - cycle%40) <= 1:
                    matrix[cycle//40][cycle%40] = '#'
                cycle += 1

            reg_x += val

    print('\n'.join([''.join(row) for row in matrix]))
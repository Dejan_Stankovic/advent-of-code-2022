class PuzzleItem():
    def __init__(self, points:set) -> None:
        self.points = points

    def left(self):
        if not all(x[0] for x in self.points):
            return
        self.points = set(map(lambda x: (x[0]-1, x[1]), self.points))
    
    def right(self):
        if not all(x[0] < 6 for x in self.points):
            return
        self.points = set(map(lambda x: (x[0]+1, x[1]), self.points))

    def down(self):
        self.points = set(map(lambda x: (x[0], x[1]-1), self.points))

    def up(self):
        self.points = set(map(lambda x: (x[0], x[1]+1), self.points))

    def shift(self, wind_arrow, reverse=False):
        if not reverse:
            if wind_arrow == '<':
                return self.left()
            else:
                return self.right()
        else:
            if wind_arrow == '<':
                return self.right()
            else:
                return self.left()


class Matrix:
    def __init__(self) -> None:
        self.points = set([(i,0) for i in range(7)])

    def is_puzzle_item_valid(self, puzzle_item):
        return len(self.points & puzzle_item.points) == 0

    def freeze_puzzle_item(self, puzzle_item):
        self.points.update(puzzle_item.points)

    def get_height(self):
        if not self.points:
            return 0
        return max([x[1] for x in self.points]) + 1

    def bottom_of_next_puzzle_item(self):
        return self.get_height() + 3



class Wind:
    def __init__(self, arrows) -> None:
        self.arrows = arrows
        self.next_move = 0
        self.num = len(self.arrows)

    def get_next_wind(self):
        val = self.arrows[self.next_move]
        self.next_move = (self.next_move + 1) % self.num
        return val


class PuzzleItems:
    def __init__(self) -> None:
        self.num_items = 5
        self.next_puzzle = 0
        self.start_y = 0
        self.generic_points = {
            0: set([(i, self.start_y) for i in range(2, 6)]),
            1: set([(3, self.start_y), (2, self.start_y+1), (3, self.start_y+1), (4, self.start_y+1), (3, self.start_y+2)]),
            2: set([(2, self.start_y), (3, self.start_y), (4, self.start_y), (4, self.start_y+1), (4, self.start_y+2)]),
            3: set([(2, self.start_y+i) for i in range(4)]),
            4: set([(2, self.start_y), (3, self.start_y), (2, self.start_y+1), (3, self.start_y+1)])
        }

    def get_next_puzzle(self, start_y):
        self.start_y = start_y
        puzzle = PuzzleItem(self.generic_points.get(self.next_puzzle, None))
        puzzle.points = set([(x[0], x[1]+self.start_y) for x in puzzle.points])
        self.next_puzzle = (self.next_puzzle + 1) % self.num_items
        return puzzle






with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))

    wind = Wind(list(rows[0]))
    matrix = Matrix()
    puzzle_items = PuzzleItems()
    prev_h = 0
    heights = []
    for i in range(175): # beginning before repetitive part
        next_puzzle = puzzle_items.get_next_puzzle(matrix.bottom_of_next_puzzle_item())

        next_puzzle.shift(wind.get_next_wind())
        while(True):
            next_puzzle.down()
            if not matrix.is_puzzle_item_valid(next_puzzle):
                next_puzzle.up()
                matrix.freeze_puzzle_item(next_puzzle)
                break
            wind_arrow = wind.get_next_wind()
            next_puzzle.shift(wind_arrow)
            if not matrix.is_puzzle_item_valid(next_puzzle):
                next_puzzle.shift(wind_arrow, reverse=True)

        tt = max([x[1] for x in matrix.points])
        print( tt - prev_h, end='')
        prev_h = tt

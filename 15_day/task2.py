
with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))
    sensor_beacon_list = []
    

    for row in rows:
        vals = row.split('=')

        sx = int(vals[1].split(',')[0])
        sy = int(vals[2].split(':')[0])

        bx = int(vals[3].split(',')[0])
        by = int(vals[4].split(':')[0])
        

        sensor_beacon_list.append((sx,sy,bx,by))
    

    sharp_points = []
    limit = 4000000
    ss = []
    for sx,sy,bx,by in sensor_beacon_list:
        manhattan = abs(sx-bx) + abs(sy-by) + 1

        for i in range(manhattan+1):
            x = sx + i
            y = sy + manhattan - i
            if 0 <= x <= limit and 0<=y <= limit:
                ss.append((x,y))

            x = sx - i
            y = sy + manhattan - i
            if 0 <= x <= limit and 0<=y <= limit:
                ss.append((x,y))

            x = sx + i
            y = sy - manhattan + i
            if 0 <= x <= limit and 0<=y <= limit:
                ss.append((x,y))

            x = sx - i
            y = sy - manhattan + i
            if 0 <= x <= limit and 0<=y <= limit:
                ss.append((x,y))
        
    
    for el in ss:
        flag = False
        for x,y, xx,yy in sensor_beacon_list:

            manh = abs(x-xx) + abs(y-yy)
            if abs(x-el[0]) + abs(y-el[1]) <= manh:
                flag = True
                break
        
        if not flag:
            x,y = el
            print(x*4000000+y)
            break
    
            

    


    # print(sharp_points[14])
    # print(sorted(sharp_points.items(), key=lambda x: len(x[1]))[0])

            
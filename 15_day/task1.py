with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))
    sensor_beacon_list = []
    

    for row in rows:
        vals = row.split('=')

        sx = int(vals[1].split(',')[0])
        sy = int(vals[2].split(':')[0])

        bx = int(vals[3].split(',')[0])
        by = int(vals[4].split(':')[0])
        


        sensor_beacon_list.append((sx,sy,bx,by))
    


    # target_row = 10
    target_row = 2000000
    sharp_points = set()

    for sx,sy,bx,by in sensor_beacon_list:
        manhattan = abs(sx-bx) + abs(sy-by)


        for j in range(sx - manhattan + abs(sy-target_row), sx + manhattan-abs(sy-target_row)):
            sharp_points.add(j)
    
    print(len(sharp_points))

            
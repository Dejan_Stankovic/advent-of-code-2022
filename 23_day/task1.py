moves = [(-1,0),(1,0),(0,-1),(0,1)] # N,S,W,E
iteration = 0

class Elf:
    def __init__(self, x,y) -> None:
        self.x = x
        self.y = y
        self.proposed_x = None
        self.proposed_y = None
    
    def propose_next_move(self, elves):
        if not self.do_i_move(elves):
            return False
        for ii in range(4):
            xx,yy = moves[(ii+iteration) % 4]
            
            x,y = self.x + xx, self.y + yy
            for elf in elves:
                if self == elf:
                    continue
                if xx != 0: # N or S
                    if x==elf.x and abs(y-elf.y) <= 1:
                        break
                elif yy != 0: # W or E
                    if y==elf.y and abs(x-elf.x) <= 1:
                        break
            else:
                self.proposed_x, self.proposed_y = x,y
                # if iteration 
                return True


    def do_i_move(self, elves):
        for elf in elves:
            if elf == self:
                continue
            neigh = [(self.x+1,self.y),(self.x-1,self.y),
                        (self.x,self.y+1),(self.x,self.y-1),
                        (self.x+1,self.y+1),(self.x+1,self.y-1),
                        (self.x-1,self.y+1),(self.x-1,self.y-1)]
            if (elf.x,elf.y) in neigh:
                return True
        return False

    def move(self):
        self.x, self.y = self.proposed_x, self.proposed_y

def print_elves(elves):
    for elf in elves:
        print(elf.x, elf.y)

with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))
    elves = []
    for i, row in enumerate(rows):
        for j, el in enumerate(list(row)):
            if el == '#':
                elves.append(Elf(i,j))

    do_next = True
    while iteration < 10:
        print(iteration)
        for elf in elves:
            prop = elf.propose_next_move(elves)
        for i in range(len(elves)):
            e1 = elves[i]
            for j in range(len(elves)):
                if i==j:
                    continue
                e2 = elves[j]
                if e1.proposed_x==e2.proposed_x and e1.proposed_y==e2.proposed_y:
                    break
            else:
                e1.move()
        for elf in elves:
            elf.proposed_x = None
            elf.proposed_y = None
            
        iteration += 1

    maxx = max(elf.x for elf in elves)
    minx = min(elf.x for elf in elves)
    maxy = max(elf.y for elf in elves)
    miny = min(elf.y for elf in elves)

    print((maxx - minx+1)*(maxy - miny+1) - len(elves))

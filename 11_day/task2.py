from collections import defaultdict

class Monkey:
    def __init__(self, index):
        self.index = index
        self.operation_operand = None
        self.test_operand = None
        self.if_test = None
        self.else_test = None
        self.items = []
    def operation(self, old):
        f = lambda old: eval(self.operation_operand)
        return  f(old)

    def test(self, xx):
        ff = lambda x: self.if_test if x % self.test_operand == 0 else self.else_test
        res = ff(xx)
        return res
        

my_map = defaultdict(int)
gen_mods = set()

with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))
    monkeys = []
    items = []
    operation = None
    test = None
    condition = None
    if_test = None
    else_test = None
    index = 0
    monkey = Monkey(index)

    for row in rows:
        if not row:
            monkeys.append(monkey)
            index += 1
            monkey = Monkey(index)
            continue
        if row.startswith('Monkey'): 
            continue
        if row.startswith('Starting'):
            splitted = row.split(':')[1].strip()
            items = list(map(int, splitted.split(', ')))
            monkey.items = items
        if row.startswith('Operation'):
            splitted = row.split(' = ')[1]
            monkey.operation_operand = splitted

        if row.startswith('Test:'):
            condition = int(row.split(' by ')[1])
        if row.startswith('If true'):
            if_test = int(row.split()[-1])
        if row.startswith('If false'):
            else_test = int(row.split()[-1])
            monkey.test_operand = condition
            monkey.if_test = if_test
            monkey.else_test = else_test
            gen_mods.add(condition)
    monkeys.append(monkey)
    
    mod = 1
    for el in gen_mods:
        mod *= el

    ll = len(monkeys)
    for tt in range(10000):
        for i in range(ll):
            monkey = monkeys[i]
            my_map[i] += len(monkey.items)
            for item in monkey.items:
                worry = monkey.operation(item)
                worry %= mod

                res = monkey.test(worry)

                monkeys[res].items.append(worry)
            monkeys[i].items = []
        



    lst = sorted(my_map.values())
    print(lst[-1]*lst[-2])
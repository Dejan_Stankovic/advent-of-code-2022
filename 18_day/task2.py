def neighbors(x,y,z): return [(x+1,y,z), (x-1,y,z), (x,y+1,z), (x,y-1,z), (x,y,z+1), (x,y,z-1)]

with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))

    points = []
    dim = 0
    for row in rows:
        point = tuple(map(int, row.split(',')))
        dim = max([dim,*point])
        points.append(point)

    faces = 6*len(points)
    for x,y,z in points:
        for nn in neighbors(x,y,z):
            if nn in points:
                faces -= 1

    visited = set()
    queue = [(dim,dim,dim)]
    while queue:
        x,y,z = point = queue.pop(0)

        if point in visited or point in points:
            continue
        visited.add(point)

        for nn in neighbors(x,y,z):
            if not all(0 <= i <= dim for i in nn): 
                continue
            queue.append(nn)

    for x in range(dim):
        for y in range(dim):
            for z in range(dim):
                point = (x,y,z)
                if point not in visited and point not in points:
                    for point2 in neighbors(x,y,z):
                        if point2 in points:
                            faces -= 1
    print(faces)

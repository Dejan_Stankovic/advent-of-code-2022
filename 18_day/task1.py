with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))

    points = []
    for row in rows:
        point = tuple(map(int, row.split(',')))
        points.append(point)

    faces = 6*len(points)
    for x,y,z in points:
        for nn in [(x+1,y,z), (x-1,y,z), (x,y+1,z), (x,y-1,z), (x,y,z+1), (x,y,z-1)]:
            if nn in points:
                faces -= 1
    print(faces)
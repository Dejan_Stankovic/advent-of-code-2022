with open('input2.txt', 'r') as f:
    rows = f.readlines()
    final_sum = 0
    for row in rows:
        row = row.strip()
        n = len(row)//2
        a = row[:n]
        b = row[n:]
        val = 0
        for letter in a:
            if letter in b:
                if letter.upper() == letter:
                    val =  ord(letter) - 64 + 26
                else:
                    val = ord(letter) - 96
                break
        final_sum += val
    print(final_sum)

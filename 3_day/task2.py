with open('input2.txt', 'r') as f:
    rows = f.readlines()
    final_sum = 0
    for i in range(0,len(rows), 3):
        a,b,c = list(map(lambda x:x.strip(), rows[i:i+3]))
        val = 0
        for letter in a:
            if letter in b and letter in c:
                if letter.upper() == letter:
                    val =  ord(letter) - 64 + 26
                else:
                    val = ord(letter) - 96
                break
        final_sum += val
    print(final_sum)

with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))
    result = 0
    for row in rows:
        first, second = row.split(',')
        a, b = map(int, first.split('-'))
        c, d = map(int, second.split('-'))
        # s1 = set(range(a, b+1))
        # s2 = set(range(c, d+1))
        # if len(s1.union(s2)) == len(s1) or len(s2.union(s1)) == len(s2):
        #     result += 1
        if a <= c and b >= d or c <= a and d >= b:
            result += 1

    print(result)
from datetime import datetime

edges = []
all_nodes = []
graph = {}
weights = {}
all_costs = {}

all_visits_in_time = {}
cnt = 0
all_paths = []

str_int_map = {}
next_int = 1



def map_str_list_to_int_list(lst):
    global next_int
    res = []
    for el in lst:
        val = str_int_map.get(el, None)
        if val == None:
            val = next_int
            str_int_map[el] = val
            next_int += 1
        res.append(val)
    return res

def visit_node(start, time, visited, current_path=[]):
    global cnt
    cnt += 1
    if cnt % 10000 == 0:
        print(cnt)
    if time == 0: return 0

    vv = [v[0] for v in visited]
    key = (start, time, frozenset(vv))
    if val:=all_visits_in_time.get(key, None):
        return val

    result = 0
    sum_ = sum(weights[el] for el in  vv)

    if start not in vv and weights[start] > 0:
        new_set = set(visited)
        new_set.add((start, time*weights[start]))
        
        result = max(result, sum_  + visit_node(start, time-1, new_set, current_path))
        my_res = sum([el[1] for el in visited])

        sss = set(map_str_list_to_int_list(current_path+[start]))
        all_paths.append([sss, my_res])
    
    for node in graph[start]:
        result = max(result, sum_ + visit_node(node, time-1, visited, current_path+[node]))

    all_visits_in_time[key] = result
    return result

with open('input1.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))
    for index, row in enumerate(rows):
        tmp = row.split(';')
        fp = tmp[0].split()
        node = fp[1]
        rate = int(fp[-1].split('=')[1])
        sp = tmp[1]
        if 'valves' in sp:
            connected = sp.split('valves ')[1].split(', ')
        else:
            connected = sp.split('valve ')[1].split(', ')

        for el in connected:
            edges.append((node, el))
        all_nodes.append(node)
        str_int_map[node] = next_int
        next_int += 1
        graph[node] = connected
        weights[node] = rate
    

    start = 'AA'
    time = 26
    visited = set()
    t1 = datetime.now()
    print(t1)
    print(visit_node(start, time, visited, ['AA']))
    t2 = datetime.now()
    print(t2, t2-t1)

    print(len(all_paths))

    target = []
    maxx = 0
    p1,p2 = None, None


    weights2 = {str_int_map[k]: v for k, v in weights.items()}
    all_paths = [(set([el for el in a if weights2[el] > 0]), b)  for a, b in all_paths]

    for i in range(len(all_paths)-1):
        for j in range(i+1, len(all_paths)):
            ss = all_paths[i][0].union(all_paths[j][0])
            if len(ss) == len(all_paths[i][0]) + len(all_paths[j][0])-1:
                if (maxxx:= max(maxx, all_paths[i][1] + all_paths[j][1])) != maxx:
                    target.append((all_paths[i], all_paths[j]))
                    maxx = maxxx
                    print(f'{maxxx=}')

    print(maxx)


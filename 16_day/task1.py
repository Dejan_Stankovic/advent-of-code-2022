from datetime import datetime

edges = []
all_nodes = []
graph = {}
weights = {}
all_costs = {}

all_visits_in_time = {}
cnt = 0

def visit_node(start, time, visited):
    if time == 0: return 0

    key = (start, time, frozenset(visited))
    if val:=all_visits_in_time.get(key, None):
        return val

    result = 0
    sum_ = sum(weights[el] for el in visited)

    if start not in visited and weights[start] > 0:
        new_set = set(visited)
        new_set.add(start)
        result = max(result, sum_  + visit_node(start, time-1, new_set))
    
    for node in graph[start]:
        result = max(result, sum_ + visit_node(node, time-1, visited))

    all_visits_in_time[key] = result
    return result

with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))
    for index, row in enumerate(rows):
        tmp = row.split(';')
        fp = tmp[0].split()
        node = fp[1]
        rate = int(fp[-1].split('=')[1])
        sp = tmp[1]
        if 'valves' in sp:
            connected = sp.split('valves ')[1].split(', ')
        else:
            connected = sp.split('valve ')[1].split(', ')

        for el in connected:
            edges.append((node, el))
        all_nodes.append(node)
        graph[node] = connected
        weights[node] = rate
    

    start = 'AA'
    time = 30
    visited = set()
    t1 = datetime.now()
    print(t1)
    print(visit_node(start, time, visited))
    t2 = datetime.now()
    print(t2, t2-t1)

with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))
    matrix = [list(row) for row in rows]
    m = len(matrix)
    n = len(matrix[0])
    queue = []
    mapped_matrix = [[0 for _ in range(n)] for _ in range(m)]

    for i in range(m):
        for j in range(n):
            if matrix[i][j] == 'S':
                mapped_matrix[i][j] = 1
                queue.append((i, j, 0))
            elif matrix[i][j] == 'E':
                mapped_matrix[i][j] = 26
            else:
                value = ord(matrix[i][j]) - 96
                mapped_matrix[i][j] = value
                if value == 1:
                    queue.append((i, j, 0))

    visited = [[False for _ in range(n)] for _ in range(m)]
    while queue:
        i,j,v = queue.pop(0)
        if visited[i][j]:
            continue
        visited[i][j] = True

        if matrix[i][j] == 'E':
            print(v)
            break
        for x, y in [(0,1), (1,0), (-1,0), (0,-1)]:
            ii = i + x
            jj = j + y
            if 0 <= ii < m and 0 <= jj < n and mapped_matrix[ii][jj] <= mapped_matrix[i][j] + 1:
                queue.append((ii, jj, v+1))
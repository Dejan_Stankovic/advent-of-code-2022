import requests

json = requests.get(
    url='https://adventofcode.com/2022/leaderboard/private/view/1027662.json',
    cookies={
        'session':'add-here-your-session-cookie-for-AoC'
    }
    )

res = json.json()
members = res.get('members', dict())
result = []
for member in members.values():
    result.append([member.get('stars', 0), a if (a:=member.get('name', '')) else 'Anonymous'])

result.sort(key=lambda x: -x[0])
print(*result, sep='\n')



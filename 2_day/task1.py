with open('input2.txt', 'r') as f:
    rows = f.readlines()
    points = 0
    
    point_map = {
        'A X': 4, 'A Y': 8, 'A Z': 3,
        'B X': 1, 'B Y': 5, 'B Z': 9,
        'C X': 7, 'C Y': 2, 'C Z': 6 
    }

    for el in rows:
        points += point_map[el.strip()]

    print(points)
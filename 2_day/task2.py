with open('input2.txt', 'r') as f:
    rows = f.readlines()
    points = 0
    
    point_map = {
        'A X': 3, 'A Y': 4, 'A Z': 8,
        'B X': 1, 'B Y': 5, 'B Z': 9,
        'C X': 2, 'C Y': 6, 'C Z': 7 
    }

    for el in rows:
        points += point_map[el.strip()]

    print(points)
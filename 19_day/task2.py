import re
from collections import deque
from datetime import datetime

t1 = datetime.now()
with open('input2.txt', 'r') as f:
    rows = list(map(lambda x: x.strip(), f.readlines()))
    blueprints = {}

    for row in rows:
        sp1, sp2 = row.split(":")
        id = int(sp1.split()[-1])
        sp2 = re.sub(r'Each | costs ', '', sp2)
        sp2 = re.sub(r' robot', '->', sp2)
        sp2 = re.sub(r' and ', ',', sp2)
        sp2 = re.sub(r'\. ', ';', sp2)
        sp2 = sp2.strip(' .')
        sp2 = re.sub(r' ', ':', sp2)
        
        robots = {}
        for robot in sp2.split(';'):
            name, costs = robot.split('->')
            cost_map = {}
            for cost in costs.split(','):
                v,k = cost.split(':')
                cost_map[k] = int(v)
            robots[name] = cost_map
        blueprints[id] = robots
    
    res = 1
    for i, bp in blueprints.items():
        if i == 4:
            break
        robot_cost = bp

        max_ore_cost = max(it['ore'] for it in robot_cost.values())
        max_clay_cost = robot_cost['obsidian']['clay']
        max_obsidian_cost = robot_cost['geode']['obsidian']
        
        robot_state = (1,0,0,0)
        item_state = (0,0,0,0)
        time_left = 32

        max_ = 0
        queue = deque([(robot_state, item_state, time_left)])
        visited = set()
        while queue:
            r_s, i_s, t = tpl = queue.popleft()

            ro,rc,rob,rg = r_s
            io,ic,iob,ig = i_s
            max_ = max(max_, ig)
            if t == 0:
                continue

            ro = min(ro, max_ore_cost)
            rc = min(rc, max_clay_cost)
            rob = min(rob, max_obsidian_cost)

            io = min(io, t*max_ore_cost - ro*(t-1))
            ic = min(ic, t*max_clay_cost - rc*(t-1))
            iob = min(iob, t*max_obsidian_cost - rob*(t-1))

            tpl2 = ((ro,rc,rob,rg),(io,ic,iob,ig), t)
            if tpl2 in visited:
                continue
            visited.add(tpl2)

            io2 = io + ro
            ic2 = ic + rc
            iob2 = iob + rob
            ig2 = ig + rg

            queue.append(((ro,rc,rob,rg),(io2,ic2,iob2,ig2), t-1))

            ore_cost_ore = robot_cost['ore']['ore']
            if io >= ore_cost_ore:
                queue.append(((ro+1,rc,rob,rg),(io2-ore_cost_ore,ic2,iob2,ig2), t-1))

            clay_cost_ore = robot_cost['clay']['ore']
            if io >= clay_cost_ore:
                queue.append(((ro,rc+1,rob,rg),(io2-clay_cost_ore,ic2,iob2,ig2), t-1))

            obs_cost_ore = robot_cost['obsidian']['ore']
            obs_cost_clay = robot_cost['obsidian']['clay']
            if io >= obs_cost_ore and ic >= obs_cost_clay:
                queue.append(((ro,rc,rob+1,rg),(io2-obs_cost_ore,ic2-obs_cost_clay,iob2,ig2), t-1))

            geo_cost_ore = robot_cost['geode']['ore']
            geo_cost_obs = robot_cost['geode']['obsidian']
            if io >= geo_cost_ore and iob >= geo_cost_obs:
                queue.append(((ro,rc,rob,rg+1),(io2-geo_cost_ore,ic2,iob2-geo_cost_obs,ig2), t-1))

        res *= max_


            
    print(res)
t2= datetime.now()
print(t2-t1)